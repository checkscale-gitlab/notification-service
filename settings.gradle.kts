pluginManagement {
    repositories {
        gradlePluginPortal()

        // Sirius repository
        maven {
            url = uri("https://gitlab.com/api/v4/groups/sirius6/-/packages/maven")
            name = "Gitlab"
        }
    }
}

rootProject.name = "notification-service"
