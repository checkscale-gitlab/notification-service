# Notification service

This service is useful to send various notifications :

* send email
* send SMS
* send webhook
* send websocket topic

## How it works

The notifications are saved in [RabbitMQ](docs/rabbitmq.md) (one queue by notification type).
The sending is asynchronous and if it fails it is retried many times until a given limit.

The application can be used with:

* [the Docker image](docs/install.md#docker)
* [the standalone JAR](docs/install.md#standalone)

To work, the application needs a [configuration file](docs/configuration.md) and a [providers descriptor](docs/providers.md#descriptor).

## How to send notification

You send notification with :

* the [API](docs/api.md) exposed by the application
* [pushing messages](docs/api.md) in RabbitMQ exchange

I recommend the push in RabbitMQ because if the application is down, the messages are persisted in RabbitMQ and
at the next application startup, the pending messages will be processed.

A [callback exchange](docs/rabbitmq.md#callback) is also used by the application. It is useful to notify the other applications when:

* a notification succeed 
* a notification failed 
* a tracking event is received

For instance, you can use the callback for billing because most providers costs money!

## The providers

Email and SMS use many providers. If the first provider fails to send the notification, the system will retry with
another provider. A simple round-robin algorithm is used.

To define the providers, a file [providers.yml](docs/providers.md#descriptor) must be placed in the same directory of the application.

* Email providers
    * [SMTP](docs/providers.md#smtp)
    * [Mailjet](docs/providers.md#mailjet-email)
    * [SendGrid](docs/providers.md#sendgrid)
    * [Mandrill](docs/providers.md#mandrill)
* SMS providers
    * [Esendex](docs/providers.md#esendex)
    * [AllMySMS](docs/providers.md#allmysms)
    * [Mailjet](docs/providers.md#mailjet-sms)

Webhook and websocket only use a single provider, so no need to configure them in the descriptor!

Tracking is also available in most email providers. Please check [providers](docs/providers.md) for more details.
