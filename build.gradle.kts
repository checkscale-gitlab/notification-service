import io.sirius.docker.DockerRestart
import io.sirius.dsl.appVersion
import io.sirius.dsl.configureJacoco
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.tasks.run.BootRun

plugins {
    id("org.springframework.boot") version "2.5.3"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    id("org.sonarqube") version "3.3"
    id("jacoco")
    id("maven-publish")
    kotlin("jvm") version "1.5.21"
    kotlin("plugin.spring") version "1.5.21"
    id("sirius") version "2.2"
}

java.sourceCompatibility = JavaVersion.VERSION_11
java.targetCompatibility = JavaVersion.VERSION_11

configureJacoco()

tasks.getByName<BootRun>("bootRun") {
    systemProperty("spring.profiles.active", "dev")
}

tasks.findByName("dockerComposeUp")?.dependsOn("assemble")

dockerCompose {
    environment = mapOf("APP_VERSION" to project.appVersion)
}

tasks.register<DockerRestart>("updateService") {
    group = "docker"
    dependsOn("assemble")
    service = "server"
}

dependencies {
    implementation(platform("org.springframework.cloud:spring-cloud-dependencies:2020.0.3"))

    // For runtime
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml")
    implementation("org.springframework.boot:spring-boot-starter-mail")
    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-amqp")
    implementation("org.springframework.boot:spring-boot-starter-websocket")
    implementation("org.springframework.boot:spring-boot-starter-reactor-netty")
    implementation("org.springframework.cloud:spring-cloud-starter-config")
    implementation("org.springframework.cloud:spring-cloud-starter-bootstrap")

    implementation("org.apache.httpcomponents:httpclient")
    implementation("org.apache.httpcomponents:httpmime")
    implementation("com.googlecode.libphonenumber:libphonenumber:8.12.29")

    // For development
    developmentOnly("org.springframework.boot:spring-boot-devtools")

    // For tests
    testImplementation("io.mockk:mockk:1.12.0")
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
        exclude(group = "org.mockito", module = "*")
    }
    testImplementation("org.springframework.security:spring-security-test")
    testImplementation("com.icegreen:greenmail:1.6.5")
}

tasks.withType<Test> {
    useJUnitPlatform()

    // Properties to test Esendex provider
    systemProperties["esendex_account"] = project.property("esendex_account")
    systemProperties["esendex_login"] = project.property("esendex_login")
    systemProperties["esendex_password"] = project.property("esendex_password")
    systemProperties["esendex_phone"] = project.property("esendex_phone")

    // Properties to test Esendex provider
    systemProperties["mailjet_apiKey"] = project.property("mailjet_apiKey")
    systemProperties["mailjet_secretKey"] = project.property("mailjet_secretKey")
    systemProperties["mailjet_from"] = project.property("mailjet_from")
    systemProperties["mailjet_to"] = project.property("mailjet_to")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "1.8"
    }
}
