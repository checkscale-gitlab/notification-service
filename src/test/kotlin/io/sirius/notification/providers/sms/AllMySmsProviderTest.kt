package io.sirius.notification.providers.sms

import com.fasterxml.jackson.databind.ObjectMapper
import com.sun.net.httpserver.HttpServer
import io.sirius.notification.model.SmsInfo
import io.sirius.notification.util.*
import org.apache.http.impl.client.CloseableHttpClient
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import java.net.InetSocketAddress
import java.net.URI
import java.util.*

class AllMySmsProviderTest {

    private lateinit var httpClient: CloseableHttpClient
    private lateinit var smsInfo: SmsInfo
    private lateinit var httpServer: HttpServer
    private lateinit var endpoint: MockEndpoint
    private lateinit var objectMapper: ObjectMapper
    private lateinit var provider: AllMySmsProvider

    @BeforeEach
    fun before() {
        objectMapper = TestUtil.objectMapper()
        endpoint = MockEndpoint(objectMapper)
        val port = 8000 + Random().nextInt(2000)

        httpServer = HttpServer.create(InetSocketAddress(port), 0)
        httpServer.createContext("/send/sms", endpoint)
        httpServer.start()

        httpClient = TestUtil.httpClient()
        provider = AllMySmsProvider(
            provider = "AllMySms",
            httpClient = httpClient,
            objectMapper = objectMapper,
            serverUri = URI.create("http://localhost:$port/send/sms"),
            credentials = PasswordCredentials("login", "pwd")
        )

        smsInfo = SmsInfo(
            from = "test",
            phoneNumber = "0606060606",
            message = "Message test"
        )
    }

    @AfterEach
    fun after() {
        httpServer.stop(0)
        httpClient.close()
    }

    @Test
    fun testSendSms() {
        endpoint.response = MockEndpoint.HttpResponse(
            HttpStatus.OK,
            AllMySmsProvider.AllMySmsResult(1, "123")
        )

        val smsCount = provider.sendSms(smsInfo)
        assertThat(smsCount).isEqualTo(1)

        assertThat(endpoint.waitForRequest()!!).satisfies { request ->
            assertThat(request.headers.getFirst(HttpHeaders.AUTHORIZATION)).isEqualTo("Basic bG9naW46cHdk")
            assertThat(request.getJsonBody<AllMySmsProvider.AllMySmsMessage>(objectMapper))
                .isEqualTo(
                    AllMySmsProvider.AllMySmsMessage(
                        to = "0606060606",
                        text = "Message test",
                        from = "test"
                    )
                )
        }
    }

    @Test
    fun testSmsFailure() {
        endpoint.response = MockEndpoint.HttpResponse(
            HttpStatus.INTERNAL_SERVER_ERROR,
            AllMySmsProvider.AllMySmsError("desc", "my_code")
        )

        assertThrows<HttpException> { provider.sendSms(smsInfo) }
    }

    @Test
    fun testSmsCount() {
        endpoint.response = MockEndpoint.HttpResponse(HttpStatus.OK, AllMySmsProvider.AllMySmsResult(5, "123"))

        val smsCount = provider.sendSms(smsInfo)
        assertThat(smsCount).isEqualTo(5)
    }
}