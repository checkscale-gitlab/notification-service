package io.sirius.notification.providers.emailing

import com.fasterxml.jackson.databind.ObjectMapper
import com.sun.net.httpserver.HttpServer
import io.sirius.notification.model.EmailInfo
import io.sirius.notification.model.ProviderMetadata
import io.sirius.notification.util.*
import org.apache.http.impl.client.CloseableHttpClient
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import java.net.InetSocketAddress
import java.net.URI
import java.util.*

class SendGridProviderTest {

    private lateinit var emailInfo: EmailInfo
    private lateinit var objectMapper: ObjectMapper
    private lateinit var httpServer: HttpServer
    private lateinit var httpClient: CloseableHttpClient
    private lateinit var endpoint: MockEndpoint
    private lateinit var provider: SendGridProvider

    @BeforeEach
    fun before() {
        httpClient = TestUtil.httpClient()
        objectMapper = TestUtil.objectMapper()
        endpoint = MockEndpoint(objectMapper)
        val port = 8000 + Random().nextInt(2000)

        httpServer = HttpServer.create(InetSocketAddress(port), 0)
        httpServer.createContext("/send/email", endpoint)
        httpServer.start()

        provider = SendGridProvider(
            provider = "SendGrid",
            httpClient = httpClient,
            credentials = BearerCredentials("4308487c-3a4b-11eb-b51e-ff85475a7208"),
            serverUri = URI.create("http://localhost:$port/send/email"),
            objectMapper = objectMapper
        )

        emailInfo = EmailInfo(
            from = EmailInfo.EmailAddress("from@test.com", "John"),
            to = listOf(EmailInfo.EmailAddress("to@test.com")),
            subject = "My subject",
            plainText = "My body",
            htmlText = "My HTML"
        )
    }

    @AfterEach
    fun after() {
        httpServer.stop(0)
        httpClient.close()
    }

    @Test
    fun testEmailSending() {
        endpoint.response = MockEndpoint.HttpResponse(HttpStatus.OK)
        provider.sendEmail(emailInfo)

        // Check payload received
        assertThat(endpoint.waitForRequest()!!).satisfies { request ->
            assertThat(request.headers.getFirst(HttpHeaders.AUTHORIZATION)).isEqualTo("Bearer 4308487c-3a4b-11eb-b51e-ff85475a7208")
            assertThat(request.getJsonBody<SendGridProvider.SendGridMessage>(objectMapper)).isEqualTo(
                SendGridProvider.SendGridMessage(
                    from = SendGridProvider.SendGridMessage.Address("from@test.com", "John"),
                    personalizations = listOf(
                        SendGridProvider.SendGridMessage.Personalization(
                            to = listOf(SendGridProvider.SendGridMessage.Address("to@test.com")),
                            cc = null,
                            bcc = null,
                            subject = "My subject",
                            custom_args = ProviderMetadata(emailInfo.id, "SendGrid", "SendGrid")
                        )
                    ),
                    content = listOf(
                        SendGridProvider.SendGridMessage.Content("text/plain", "My body"),
                        SendGridProvider.SendGridMessage.Content("text/html", "My HTML")
                    ),
                    attachments = null
                )
            )
        }
    }

    @Test
    fun testRejected() {
        endpoint.response = MockEndpoint.HttpResponse(
            HttpStatus.FORBIDDEN,
            SendGridProvider.SendGridErrors(emptyList())
        )

        val ex = assertThrows<HttpException> { provider.sendEmail(emailInfo) }
        assertThat(ex.status).isEqualTo(HttpStatus.FORBIDDEN)
    }
}