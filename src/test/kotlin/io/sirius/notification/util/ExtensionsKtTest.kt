package io.sirius.notification.util

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class ExtensionsKtTest {

    @Test
    fun testListDuplicate() {
        assertThat(listOf("a", "b", "c").findDuplicates()).isEmpty()
        assertThat(listOf("a", "b", "c", "b", "a").findDuplicates()).containsExactlyInAnyOrder("a", "b")
    }
}