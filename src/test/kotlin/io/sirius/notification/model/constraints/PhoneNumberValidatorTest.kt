package io.sirius.notification.model.constraints

import io.mockk.mockk
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import javax.validation.ConstraintValidatorContext

internal class PhoneNumberValidatorTest {

    private lateinit var validator: PhoneNumberValidator
    private lateinit var context: ConstraintValidatorContext

    @BeforeEach
    fun before() {
        validator = PhoneNumberValidator()
        context = mockk(relaxed = true)
    }

    @Test
    fun testFrenchPhoneNumberFormat() {
        assertThat(validator.isValid("+33606060606", context)).isTrue() // Mobile
        assertThat(validator.isValid("+33406060606", context)).isFalse() // Fixed
        assertThat(validator.isValid("0606060606", context)).isFalse() // Fixed
        assertThat(validator.isValid("+3360606060", context)).isFalse()
        assertThat(validator.isValid("+336060606060", context)).isFalse()
    }
}