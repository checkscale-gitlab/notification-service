package io.sirius.notification.model

import com.fasterxml.jackson.annotation.JsonIgnore

interface NotificationInfo {

    val id: String
    val metadata: Map<String, String>

    @get:JsonIgnore
    val type: Type

    enum class Type { EMAIL, SMS, WEBHOOK, WEBSOCKET }

}

