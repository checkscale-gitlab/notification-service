package io.sirius.notification.model

import java.util.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

data class WebSocketInfo(
    @field:Size(max = 64)
    override val id: String = UUID.randomUUID().toString(),

    override val metadata: Map<String, String> = emptyMap(),

    @field:NotBlank
    val topic: String,

    @field:NotNull
    val body: Any

) : NotificationInfo {

    override val type = NotificationInfo.Type.WEBSOCKET
}
