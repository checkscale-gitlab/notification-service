package io.sirius.notification.model

import java.util.*
import javax.activation.FileTypeMap
import javax.validation.constraints.*

data class EmailInfo(
    @field:Size(max = 64)
    override val id: String = UUID.randomUUID().toString(),

    override val metadata: Map<String, String> = emptyMap(),

    @field:NotNull
    val from: EmailAddress,

    @field:Size(min = 1, max = 200)
    val subject: String,
    val plainText: String? = null,
    val htmlText: String? = null,

    @field:NotNull
    @field:NotEmpty
    val to: List<EmailAddress>,
    val cc: List<EmailAddress> = emptyList(),
    val bcc: List<EmailAddress> = emptyList(),
    val attachments: List<Attachment> = emptyList(),

    val providers: Set<String> = emptySet()
) : NotificationInfo {

    override val type = NotificationInfo.Type.EMAIL

    data class EmailAddress(
        @field:Email
        @field:NotBlank
        val email: String,

        @field:Size(max = 100)
        val personal: String? = null
    )

    data class Attachment(
        @field:Size(min = 5, max = 100)
        @field:NotBlank
        @field:Pattern(regexp = """^[a-zA-Z0-9-_ ]+\.[a-zA-Z0-9]+""")
        val filename: String,

        @field:Pattern(regexp = "^[a-z]+/[a-z-]+$")
        val contentType: String = FileTypeMap.getDefaultFileTypeMap().getContentType(filename),

        @field:NotNull
        @field:Size(min = 1, max = 10_485_760)
        val data: ByteArray
    ) {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            require(other is Attachment)
            return Objects.equals(filename, other.filename) &&
                    Objects.equals(contentType, other.contentType) &&
                    Objects.equals(data, other.data)
        }

        override fun hashCode(): Int {
            var result = filename.hashCode()
            result = 31 * result + contentType.hashCode()
            result = 31 * result + data.contentHashCode()
            return result
        }
    }

}