package io.sirius.notification.services

import io.sirius.notification.providers.NotificationProvider

interface ProviderService<T : NotificationProvider> {

    /** Get all available providers */
    val providers: List<T>

    /**
     * Select a provider in the list following "round robin" algorithm
     *
     * @param index The position to use in the list. If it is over the list size limit it restarts the list
     * @param requestedProvidersIds The providers to use (must be part of [providers]). If empty, all available providers are used
     */
    fun getProvider(
        index: Int,
        requestedProvidersIds: Set<String>
    ): T

}