package io.sirius.notification.services.impl

import io.sirius.notification.config.RabbitConfig.Companion.HEADER_X_DEATH
import io.sirius.notification.model.SmsInfo
import io.sirius.notification.providers.sms.SmsProvider
import io.sirius.notification.services.ExchangeService
import io.sirius.notification.services.NotificationService
import io.sirius.notification.services.ProviderService
import io.sirius.notification.util.getTryIndex
import io.sirius.notification.util.logger
import io.sirius.notification.util.mdc
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.messaging.handler.annotation.Header
import org.springframework.stereotype.Service

@Service
class SmsServiceImpl(
    private val providerService: ProviderService<SmsProvider>,
    private val exchangeService: ExchangeService
) : NotificationService<SmsInfo> {

    private val logger = logger()

    @RabbitListener(
        queues = ["#{@smsQueue.name}"],
        id = "notification-sms"
    )
    override fun processNotification(
        notification: SmsInfo,
        @Header(HEADER_X_DEATH) xDeathHeader: List<Map<String, *>>?
    ) {
        mdc {
            notification.mdc()
            val retryIndex = xDeathHeader.getTryIndex()
            val provider = providerService.getProvider(retryIndex, notification.providers)
            val smsCount = provider.sendSms(notification)
            logger.info("SMS sent")

            exchangeService.sendSuccessCallback(
                notification = notification,
                provider = provider,
                tryIndex = retryIndex,
                count = smsCount
            )
        }
    }
}