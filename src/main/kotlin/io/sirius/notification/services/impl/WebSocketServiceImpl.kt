package io.sirius.notification.services.impl

import io.sirius.notification.config.RabbitConfig.Companion.HEADER_X_DEATH
import io.sirius.notification.model.WebSocketInfo
import io.sirius.notification.services.ExchangeService
import io.sirius.notification.services.NotificationService
import io.sirius.notification.util.getTryIndex
import io.sirius.notification.util.logger
import io.sirius.notification.util.mdc
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.messaging.handler.annotation.Header
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Service

@Service
class WebSocketServiceImpl(
    private val exchangeService: ExchangeService,
    private val messagingTemplate: SimpMessagingTemplate
) : NotificationService<WebSocketInfo> {

    private val logger = logger()

    @RabbitListener(
        queues = ["#{@webSocketQueue.name}"],
        id = "notification-websocket"
    )
    override fun processNotification(
        notification: WebSocketInfo,
        @Header(HEADER_X_DEATH) xDeathHeader: List<Map<String, *>>?
    ) {
        mdc {
            notification.mdc()
            val retryIndex = xDeathHeader.getTryIndex()
            messagingTemplate.convertAndSend("/topic/${notification.topic}", notification.body)
            logger.info("Websocket message sent to topic: {}", notification.topic)

            exchangeService.sendSuccessCallback(
                notification = notification,
                tryIndex = retryIndex,
                provider = null
            )
        }
    }
}