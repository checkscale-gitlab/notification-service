package io.sirius.notification.services.impl

import io.sirius.notification.config.NotificationProperties
import io.sirius.notification.model.NotificationInfo
import io.sirius.notification.services.DeadLetterService
import io.sirius.notification.services.ExchangeService
import io.sirius.notification.util.getDeathHeaders
import io.sirius.notification.util.logger
import io.sirius.notification.util.mdc
import org.springframework.amqp.core.Message
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.stereotype.Service


@Service
class DeadLetterServiceImpl(
    private val template: RabbitTemplate,
    private val notificationExchangeName: String,
    private val exchangeService: ExchangeService,
    private val properties: NotificationProperties
) : DeadLetterService {

    private val logger = logger()

    @RabbitListener(
        queues = ["#{@deadLetterQueue.name}"],
        concurrency = "1", // No need concurrency here because the dead letter process is very fast
        id = "dead-letter"
    )
    override fun onFailedNotification(failedMessage: Message) {
        val deathHeader = failedMessage.getDeathHeaders().first()
        val retriesCnt = deathHeader.count

        val notification = template.messageConverter.fromMessage(failedMessage)
        require(notification is NotificationInfo)

        mdc {
            notification.mdc()
            if (retriesCnt > properties.maxTriesCount) {
                // Last retry ... sending to error callback
                logger.error("Discarding notification")
                exchangeService.sendErrorCallback(notification, retriesCnt)

            } else {
                // Requeue
                logger.info("Retrying message for the {} time", retriesCnt)

                template.send(
                    notificationExchangeName,
                    failedMessage.messageProperties.receivedRoutingKey,
                    failedMessage
                )
            }
        }
    }
}