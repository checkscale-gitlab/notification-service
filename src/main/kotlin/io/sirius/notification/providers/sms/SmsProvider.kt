package io.sirius.notification.providers.sms

import io.sirius.notification.model.SmsInfo
import io.sirius.notification.providers.NotificationProvider

interface SmsProvider : NotificationProvider {

    /**
     * Send a SMS
     *
     * @param smsInfo The SMS to send
     * @return The SMS count
     */
    fun sendSms(smsInfo: SmsInfo): Int

}