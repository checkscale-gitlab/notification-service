package io.sirius.notification.providers.sms

import com.fasterxml.jackson.databind.ObjectMapper
import io.sirius.notification.model.SmsInfo
import io.sirius.notification.providers.ProviderConfig
import io.sirius.notification.util.logger
import org.apache.http.impl.client.CloseableHttpClient
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.JavaMailSenderImpl
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.mail.javamail.MimeMessagePreparator
import java.nio.charset.StandardCharsets
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

/**
 * THIS PROVIDER MUST ONLY BE USED IN TESTS!!!
 */
class DummySmsProvider(
    override val provider: String,
    override val id: String = provider,
    private val sender: JavaMailSender
) : SmsProvider {

    private val logger = logger()

    override fun sendSms(smsInfo: SmsInfo): Int {
        logger.warn("*** Send SMS with dummy provider ***")
        sender.send(SmsMessagePreparator(smsInfo))
        return 1
    }

    private class SmsMessagePreparator(val smsInfo: SmsInfo) : MimeMessagePreparator {

        override fun prepare(mimeMessage: MimeMessage) {
            val helper = MimeMessageHelper(mimeMessage, false)
            helper.setFrom(InternetAddress("dummy@sirius.com", smsInfo.from, "UTF-8"))
            helper.setSubject("SMS")
            helper.setTo(InternetAddress("dummy${smsInfo.phoneNumber}@sirius.com", smsInfo.from, "UTF-8"))
            helper.setText(smsInfo.message, false)
        }
    }
}

data class DummySmsProviderConfig(
    override val provider: String,
    override val id: String = provider,
    val host: String,
    val port: Int
) : ProviderConfig<DummySmsProvider> {

    override fun createProvider(
        httpClient: CloseableHttpClient,
        objectMapper: ObjectMapper
    ): DummySmsProvider {
        val sender = JavaMailSenderImpl()
        sender.host = host
        sender.port = port
        sender.protocol = "smtp"
        sender.defaultEncoding = StandardCharsets.UTF_8.name()

        return DummySmsProvider(
            id = id,
            provider = provider,
            sender = sender
        )
    }

}