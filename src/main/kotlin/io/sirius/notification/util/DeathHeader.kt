package io.sirius.notification.util

/** Represents the `x-death` header in RabbitMQ */
data class DeathHeader(
    val reason: String,
    val count: Int,
    val exchange: String,
    val routingKeys: Set<String>,
    val queue: String
)