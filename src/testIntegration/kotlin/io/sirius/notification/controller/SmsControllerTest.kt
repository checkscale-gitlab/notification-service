package io.sirius.notification.controller

import com.icegreen.greenmail.util.GreenMail
import com.icegreen.greenmail.util.ServerSetup
import io.sirius.notification.AbstractTest
import io.sirius.notification.model.NotificationCallback
import io.sirius.notification.model.NotificationInfo
import io.sirius.notification.model.NotificationSuccessCallback
import io.sirius.notification.model.SmsInfo
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.concurrent.TimeUnit

class SmsControllerTest : AbstractTest() {

    private lateinit var smtpServer: GreenMail
    private lateinit var smsInfo: SmsInfo

    @BeforeEach
    fun before() {
        callbackDeque.clear()

        smtpServer = GreenMail(ServerSetup(26000, null, ServerSetup.PROTOCOL_SMTP))
        smtpServer.start()

        smsInfo = SmsInfo(
            id = "429d35e8-37e2-11eb-96a6-037b3a1b8fbd",
            from = "test",
            phoneNumber = "+33606060606",
            message = "My message"
        )
    }

    @AfterEach
    fun after() {
        smtpServer.stop()
    }

    @Test
    fun testSmsSending() {
        mockMvc.perform(
            post("/api/sms")
                .with(httpBasic("sirius","sirius"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload(smsInfo))
        ).andExpect(status().isOk)

        val callback: NotificationCallback = callbackDeque.pollLast(5, TimeUnit.SECONDS)!!
        assertThat(callback).isEqualTo(
            NotificationSuccessCallback(
                notification = NotificationCallback.NotificationCallbackInfo(
                    id = "429d35e8-37e2-11eb-96a6-037b3a1b8fbd",
                    type = NotificationInfo.Type.SMS
                ),
                counter = 1,
                tryIndex = 0,
                provider = NotificationCallback.ProviderCallbackInfo(
                    id = "dummySms",
                    type = "dummySms"
                )
            )
        )
        assertThat(callbackErrorDeque).isEmpty()
    }

    @Test
    fun testInvalidParameters() {
        assertBadRequest(smsInfo.copy(phoneNumber = "0606060606"))
    }

    private fun assertBadRequest(smsInfo: SmsInfo) {
        mockMvc.perform(
            post("/api/sms")
                .with(httpBasic("sirius","sirius"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload(smsInfo))
        ).andExpect(status().isBadRequest)
    }
}