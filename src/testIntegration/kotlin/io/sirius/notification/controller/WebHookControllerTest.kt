package io.sirius.notification.controller

import com.sun.net.httpserver.HttpServer
import io.sirius.notification.AbstractTest
import io.sirius.notification.model.NotificationCallback
import io.sirius.notification.model.NotificationInfo
import io.sirius.notification.model.NotificationSuccessCallback
import io.sirius.notification.model.WebHookInfo
import io.sirius.notification.util.DummyPayload
import io.sirius.notification.util.HttpCallback
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.net.InetSocketAddress
import java.net.URI
import java.util.*
import java.util.concurrent.TimeUnit

class WebHookControllerTest : AbstractTest() {

    private var port: Int = 8000
    private lateinit var httpCallback: HttpCallback
    private lateinit var httpServer: HttpServer

    @BeforeEach
    fun before() {
        callbackDeque.clear()

        httpCallback = HttpCallback()
        port = 8000 + Random().nextInt(2000)
        httpServer = HttpServer.create(InetSocketAddress(port), 0)
        httpServer.createContext("/api", httpCallback)
        httpServer.start()
    }

    @AfterEach
    fun after() {
        httpServer.stop(0)
    }

    @Test
    fun testSendStringWebHook() {
        val webHookInfo = WebHookInfo(
            remoteUri = URI.create("http://localhost:$port/api"),
            headers = listOf(WebHookInfo.Header("x-test", "my-test")),
            method = WebHookInfo.Method.POST,
            body = "Hello dude é@ç!ù"
        )

        // Send webhook
        mockMvc.perform(
            post("/api/webhook")
                .with(httpBasic("sirius", "sirius"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload(webHookInfo))
        ).andExpect(MockMvcResultMatchers.status().isOk)

        assertThat(httpCallback.waitForResponse()).satisfies { rsp ->
            assertThat(rsp.headers.getFirst("x-test")).isEqualTo("my-test")
            assertThat(rsp.method).isEqualTo("POST")
            assertThat(rsp.body).isEqualTo("Hello dude é@ç!ù")
        }
    }

    @Test
    fun testSendJsonWebHook() {
        val webHookInfo = WebHookInfo(
            id = "07504d0e-37e2-11eb-9ff7-b374c64686bc",
            remoteUri = URI.create("http://localhost:$port/api"),
            headers = listOf(WebHookInfo.Header("x-test", "my-test")),
            method = WebHookInfo.Method.POST,
            body = DummyPayload()
        )

        // Send webhook
        mockMvc.perform(
            post("/api/webhook")
                .with(httpBasic("sirius", "sirius"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload(webHookInfo))
        ).andExpect(MockMvcResultMatchers.status().isOk)

        assertThat(httpCallback.waitForResponse()).satisfies { rsp ->
            assertThat(rsp.headers.getFirst("x-test")).isEqualTo("my-test")
            assertThat(rsp.method).isEqualTo("POST")
            assertThat(rsp.body).isEqualTo("""{"firstName":"john","lastName":"doe","email":{"value":"john@doe.com"}}""")
        }

        val callback: NotificationCallback = callbackDeque.pollLast(5, TimeUnit.SECONDS)!!
        assertThat(callback).isEqualTo(
            NotificationSuccessCallback(
                notification = NotificationCallback.NotificationCallbackInfo(
                    id = "07504d0e-37e2-11eb-9ff7-b374c64686bc",
                    type = NotificationInfo.Type.WEBHOOK
                ),
                counter = 1,
                tryIndex = 0,
                provider = null
            )
        )
    }

    @Test
    fun testSendWebHookWithoutBody() {
        val webHookInfo = WebHookInfo<Unit>(
            id = "68fc6382-37e0-11eb-8423-53963871b8ca",
            remoteUri = URI.create("http://localhost:$port/api"),
            headers = listOf(WebHookInfo.Header("x-test", "my-test")),
            method = WebHookInfo.Method.POST
        )

        // Send webhook
        mockMvc.perform(
            post("/api/webhook")
                .with(httpBasic("sirius", "sirius"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload(webHookInfo))
        ).andExpect(MockMvcResultMatchers.status().isOk)

        assertThat(httpCallback.waitForResponse()).satisfies { rsp ->
            assertThat(rsp.headers.getFirst("x-test")).isEqualTo("my-test")
            assertThat(rsp.method).isEqualTo("POST")
            assertThat(rsp.body).isEmpty()
        }

        val callback: NotificationCallback = callbackDeque.pollLast(5, TimeUnit.SECONDS)!!
        assertThat(callback).isEqualTo(
            NotificationSuccessCallback(
                notification = NotificationCallback.NotificationCallbackInfo(
                    id = "68fc6382-37e0-11eb-8423-53963871b8ca",
                    type = NotificationInfo.Type.WEBHOOK
                ),
                counter = 1,
                tryIndex = 0,
                provider = null
            )
        )
    }

    private fun Map<String, List<String>>.getFirst(key: String): String? {
        val values: List<String> = get(key) ?: return null
        return values.getOrNull(0)
    }

}