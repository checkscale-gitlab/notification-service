package io.sirius.notification.util

import org.springframework.messaging.simp.stomp.StompCommand
import org.springframework.messaging.simp.stomp.StompHeaders
import org.springframework.messaging.simp.stomp.StompSession
import org.springframework.messaging.simp.stomp.StompSessionHandler
import java.lang.reflect.Type
import java.time.Duration
import java.util.concurrent.BlockingDeque
import java.util.concurrent.LinkedBlockingDeque
import java.util.concurrent.TimeUnit

class StompSessionHandlerImpl<T : Any>(private val type: Type) : StompSessionHandler {

    private val queue: BlockingDeque<T> = LinkedBlockingDeque()

    override fun afterConnected(session: StompSession, connectedHeaders: StompHeaders) {
        // noop
    }

    override fun handleException(
        session: StompSession,
        command: StompCommand?,
        headers: StompHeaders,
        payload: ByteArray,
        exception: Throwable
    ) {
        exception.printStackTrace()
    }

    override fun handleTransportError(session: StompSession, exception: Throwable) {
        exception.printStackTrace()
    }

    override fun getPayloadType(headers: StompHeaders): Type = type

    @Suppress("UNCHECKED_CAST")
    override fun handleFrame(headers: StompHeaders, payload: Any?) {
        queue.addFirst(payload as T)
    }

    fun waitForMessage(timeout: Duration = Duration.ofSeconds(3)): T? =
        queue.pollLast(timeout.seconds, TimeUnit.SECONDS)
}